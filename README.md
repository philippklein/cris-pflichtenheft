# Pflichtenheft/Angebot: CRIS

| | |
| -- | -- |
| **Projektbezeichnung** | CRIS |
| **Projektleiter** | Philipp Klein |
| **Erstellt am** | 11.08.2022 |
| **Letzte Aenderung am** | 11.08.2022 |
| **Aktuelle Version** | 1.0 |

## 1. Allgemeines

### 1.1 Kenndaten

* Programmiersprache: Python (Backend) + wenig Javascript (Frontend)
* Sprachen: Deutsch und Englisch, leicht um weitere Sprachen zu erweitern
* Datenbanksystem: PostgreSQL
* Ziel-OS: Ubuntu 22.04
* Open Source, veröffentlicht in einem FAU-Gitlab

### 1.2 Abkürzungen und Ausdrücke

* (Pfl): Pflicht
* (Opt): Optional
* Production Server: Das Produktivsystem, auf dem CRIS läuft.
* Staging Server: Ein Testserver, der dem Production Server sehr ähnlich ist. Dient dem Erproben von Features und dem Austausch mit dem Auftraggeber

## 2. Funktionale Anforderungen

### 2.1 Abbildung der Funktionen des aktuellen CRIS-Systems (Pfl)

Alle relavanten und gewünschten Funktionalitäten des aktuellen CRIS-Systems werden übernommen. Beim logischen Aufbau der Seite wird sich ebenfalls am aktuellen System orientiert.

### 2.2 Tagesaktualität der Daten (Pfl)

Die Daten werden mindestens einmal täglich über die XML-Schnittstelle mit der CRIS-Datenbank synchronisiert. Es ist zu prüfen, ob, abhängig von der Zeitdauer, eine mehrmalige Synchronisation oder eine Teilsynchronisation mehrmals am Tag möglich ist.

### 2.3 Mehrsprachigkeit der Oberfläche (Pfl)

Die CRIS-Weboberfläche wird in Deutsch und Englisch verfügbar sein. Die Sprache wird aus den Browsereinstellungen des Nutzers übernommen. Dem Nutzer ist es möglich, die Sprache innerhalb der Weboberfläche umzustellen. Das System kann im Sourcecode um weitere Sprachen erweitert werden.

### 2.4 Erweiterte Such- und Filterfunktionen (Pfl)

Innerhalb der verschiedenen Kategorien (Publikationen, Personen usw.) wird es möglich sein, die gefundenen Ergebnisse nach sinnvollen Kriterien zu filtern. Diese Filterkriterien werden zusammen mit dem Auftraggeber erarbeitet und in Mockups dokumentiert.

### 2.5 Persistende oder Semi-Persistente Nutzereinstellungen (Pfl)

Neben der Spracheinstellung wird der Nutzer in der Lage sein, weitere Einstellungen vorzunehmen. Diese Einstellungen sind an die Session gebunden und verschwinden, wenn die Seite verlassen wird. Sollte der Nutzer der Nutzung von Cookies zustimmen, werden die Einstellungen an die Cookies gebunden und sind so längerfristig gespeichert.

### 2.6 Freitextsuche (Opt)

Zusätzlich zur gezielten Auswahl von Filterkriterien (z.B. einer Jahreszahl) wird eine Freitextsuche möglich sein. Diese wird versuchen, den vom Nutzer eingegeben Freitext zu interpretieren und die einzelnen Bestandteile den vorher definierten Filterkriterien zuzuweisen. Der Nutzer sieht im Anschluss die Zuordnung und kann diese ggf. korrigieren. 

Aufgrund der Menge an möglichen Eingaben wird es hier vermutlich notwendig ein, eine gewisse Form vorzugeben, z.B. *Publikationsname Jahr Autorennamen*

### 2.7 Logische Verknüpfung von Filterfunktionen (Opt)

Es wird möglich sein, Filterfunktionen mit logischen Operatoren (AND, OR) zu verknüpfen. Bei der Verknüpfung wird sich auf einzelne Filterkriterien beschränkt, z.B. *Autor A OR Autor B*. Verschiedene Filterkriterien (z.B. Autor und Jahr) sind wahlweise immer AND- oder OR-verknüpft.

### 2.8 Uebersichtskarte der Kooperationen (Opt)

Für Publikationen und Personen wird eine Uebersichtskarte geben. Auf dieser ist zu sehen, welche Einrichtungen weltweit an der Publikation beteiligt waren bzw. mit welchen Einrichtungen die Person schon kooperiert hat.

## 3. Nicht-Funktionale Anforderungen

### 3.1 Allgemeine Anforderungen

* Schnelle Antwortzeiten. Standard-Anfragen sollten deutlich weniger als 1s Antwortzeit benötigen.
* Aktuelles Corporate Design der FAU wird eingehalten
* Modernes Design der einzelnen Unterseiten und der Hauptseite.
* Anpassung der Templates und Designs im Sourcecode möglich. Hierfür wird die Django template language genutzt.
* Keine weiteren Lizenzkosten für verwendete Software
* Keine Offline-Zeiten während der Synchronisation der Daten

### 3.2 Gesetzliche Anforderungen

* Freigabe durch den FAU-Datenschutzbeauftragten
* Impressum und Datenschutzerklärung
* Barrierefrei nach WCAG 2.0 und BITV 2
* Open-Source-Lizenz in Absprache mit dem Auftraggeber
* Cookie-Abfrage

### 3.3 Technische Anforderungen

* Hohe Testabdeckung der relevanten Codebestandteile. Zielgrösse sind 90%. Das Minimum von 85% ist nicht zu unterschreiten.
* Aktuelle Versionen von sämtlichen Softwarekomponenten
* Monitoring der Anwendung
* Staging-Server zur Kontrolle durch Entwickler und Auftraggeber
* Continous Integration und Continous Delivery zur kontinuierlichen Qualitätskontrolle.

## 4. Rahmenbedingungen

### 4.1 Phasen

Das Projekt unterteilt sich in 3 Entwicklungsphasen sowie die anschliessende Maintenance-Phase

**Phase 1:** Minumum Viable Product

In dieser Phase wird die alte CRIS-Version neu implementiert. Ziel dieser Phase ist es, das alte CRIS möglichst schnell durch eine bessere, schnellere Variante abzulösen, ohne dabei auf Funktionalität verzichten zu müssen.

**Phase 2:** Pflichtfunktionalität

In dieser Phase werden alle Pflichtfunktionalitäten umgesetzt. Im Rahmen dieser Projektphase werden die einzelnen Unterseiten designtechnisch überarbeitet, sofern dies nicht schon in Phase 1 geschehen ist.

**Phase 3:** Optionale Funktionalitäten

In dieser Phase werden die als optional gekennzeichnenten Funktionalitäten umgesetzt.

### 4.2 Zeitplan

Angestrebte Abschlüsse der einzelnen Phasen:

* Phase 1: Dezember 2022
* Phase 2: Februar 2022
* Phase 3: März 2022

### 4.3 Technische Anforderungen

Vom Auftraggeber werden benötigt:

* Zugriff auf alle relevanten Daten über die XML-Schnittstelle
* Kooperation/Austausch bei der Erstellung von Mockups für die einzelnen Seiten der Applikation
* Freigabe der Subdomain staging.cris.fau.de auf eine IP-Adresse unserer Wahl (innerhalb des FAU-Netzes)

## 5. Liefer- und Abnahmebedingungen

### 5.1 Preis

Der Gesamtpreis der oben beschriebenen Funktionalitäten beträgt 50.000 Euro. Dieser Preis ist in 3 Abschlägen zu zahlen, jeweils nach Beendigung einer Projektphase und deren Übergabe/Abnahme:

* Phase 1: 20.000
* Phase 2: 20.000
* Phase 3: 10.000

Sollte eine Projektphase nicht vollumfänglich fertiggestellt werden können, so ist diese nur anteilig zu bezahlen.

### 5.2 Abnahme

Im Rahmen der Entwicklung erfolgt eine kontinuierliche Zwischenabnahme durch den Auftraggeber. Diese sich wiederholende Zwischenabnahme geschieht im Rahmen von wiederkehrenden Meetings zwischen Auftraggebern und Auftragnehmern. Dem Auftraggeber werden hier die neuen Features präsentiert. Ueber den Staging-Server hat dieser die Möglichkeit, kontinuierlich den aktuellen Projektstand zu prüfen.

Vom Auftraggeber freigegebene Features werden auf das Produktivsystem übertragen.

Die Endabnahme erfolgt im Rahmen eines Vortrags, zu dem auch die Stakeholder eingeladen sind.

## 6. Maintenance

Dieses Angebot umfasst eine 1-monatige Maintenance-Phase. Im Rahmen dieser werden weiterhin selbstverschuldete Fehler behoben und ausstehende Features beendet.

Im Anschluss hat der Auftraggeber die Wahl zwischen 3 Maintenance/Support-Modellen:

### 6.1 Keine Maintenance + Support on Demand

In diesem Modell ist der Auftraggeber selbst für das Hosting und Updates zuständig. Sollte Support durch den Auftragnehmer notwendig sein, so wird dieser auf Stundenbasis zu einem Stundenlohn von 100 Euro/h verrechnet, mit einer Minimalzeit von 10 Stunden pro Monat.

### 6.2 Maintenance + Basic Support

**Kosten**: 2000 Euro pro Jahr, im Voraus zu zahlen

In diesem Modell übernimmt der Auftragnehmer das Hosting des Staging und Production Servers. Er sorgt dafür, dass die verwendete Software auf einem aktuellen Stand gehalten wird und das die Applikation erreichbar ist. Selbstverschuldete Fehler/Bugs werden behoben. Wir unterstützen den Auftraggeber dabei , kleinere Aenderungen selbst durchzuführen.

### 6.3 Maintenance + Full Support

**Kosten**: 7000 Euro pro Jahr, im Voraus zu zahlen

Dieser Modell entspricht dem Modell aus 6.2. Bei diesem Modell sind aber 100 Stunden pro Jahr für Anpassungen und Featurewünsche inkludiert.
